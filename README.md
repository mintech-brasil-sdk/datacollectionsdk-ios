# Data Collection SDK

**Data Collection SDK** is a technical solution for collecting some end user and mobile device data in the host application with minimal influence on the user experience. It can collect more than 37 types variables such as device name, battery status, etc. (check Data Dictionary for a complete list of collected data and their required permissions).
Mintech will provide credentials (app_id and app_secret) based on your package name in App Store.

https://mintechbrasil.com.br/

© Todos os direitos reservados Mintech - 2021
